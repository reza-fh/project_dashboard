<?php

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($curl);
curl_close($curl);

$result = json_decode($result, true);
$global = $result['Global'];
$temp_NewConfirmed = (string)$global['NewConfirmed'];
$temp_TotalConfirmed = (string)$global['TotalConfirmed'];
$temp_NewDeaths = (string)$global['NewDeaths'];
$temp_TotalDeaths = (string)$global['TotalDeaths'];
$temp_NewRecovered = (string)$global['NewRecovered'];
$temp_TotalRecovered = (string)$global['TotalRecovered'];
$countries = $result['Countries'];
$temp_plus = $temp_TotalConfirmed + $temp_TotalDeaths + $temp_TotalRecovered;
$temp_Recovered = $temp_TotalRecovered/$temp_TotalConfirmed*100;
$temp_Deaths = $temp_TotalDeaths/$temp_TotalConfirmed*100;
$temp_arr = count($countries);

?>

<!doctype html>
<html lang='en'>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
   <link rel="shortcut icon" href="img/logo-2.png" type="image/x-icon">
    <title>Dashboard || Covid-19</title>
    <style>
      .miror {
        background-color: white;
        width: 220px;
        height: 555px;
        /* border: 30px; */
        overflow: scroll;
      }
    </style>
  </head>
  <body>

<div class="container-fluid mt-2">
  <div class="row">
    <div class="col-12">
    <nav class="navbar navbar-light border border-dark">
      <h6 class="navbar-brand">
        <img src="img/logo-2.png" width="50" height="50" alt="" loading="lazy">
       Dashboard Covid-19 world
    </h6>
      <div>
        <a href="https://github.com/reza-fh"><i class="fab fa-github" style="font-size: 30px; margin-right: 20px; color: black;"></i></a>
        <a href="https://gitlab.com/reza-fh"><i class="fab fa-gitlab" style="font-size: 30px; margin-right: 10px; color: black;"></i></a>
        <span><?php echo date('d F Y'); ?></span>
      </div>
    </nav>
    </div>
    <div class="col-2">
      <div class="miror" style="margin-top: 10px;">
        <div class="col-12 float-left">
          <h4 class="mx-auto mt-2">State Data</h4>
          <hr>
          <?php foreach($countries as $key): ?>
            <p class="mt-1"> <?php echo $key['Country']; ?></p>
            <small>Confirmed : <?php echo $key['TotalConfirmed']; ?></small><br>
            <small>Recovered : <?php echo $key['TotalRecovered']; ?></small><br>
            <small>Death     : <?php echo $key['TotalDeaths']; ?></small><br>
            <small>Date : <?php echo $key['Date']; ?></small>
            <hr>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <div class="col-7 mt-2">
    <div class="card-group">
      <div class="card">
        <div class="card-body bg-secondary">
          <div class="col-md-4 float-left">
            <i class="fa fa-bed" style="font-size: 80px; opacity: 0.5;"></i>
          </div>
          <div class="col-md-8 float-left">
            <center><h5 class="card-title text-light">Total Confirmed</h5></center>
            <center><p class="card-text text-light"><?php echo number_format($temp_TotalConfirmed); ?></p></center>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-body bg-success">
        <div class="col-md-4 float-left">
            <i class="fa fa-child" style="font-size: 80px; opacity: 0.5;"></i>
          </div>
          <div class="col-md-8 float-left">
            <center><h5 class="card-title text-light">Total Recovered</h5></center>
            <center><p class="card-text text-light"><?php echo number_format($temp_TotalRecovered); ?></p></center>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-body bg-danger">
        <div class="col-md-4 float-left">
            <i class="fal fa-frown" style="font-size: 80px; opacity: 0.5;"></i>
          </div>
          <div class="col-md-8 float-left">
            <center><h5 class="card-title text-light">Total<br>Deaths</h5></center>
            <center><p class="card-text text-light"><?php echo number_format($temp_TotalDeaths); ?></p></center>
          </div>
        </div>
      </div>
      </div>
  <div class="container">
        <div class="row">
            <div class="col">
                <center>
                    <a href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span aria-hidden="true"><i class="fas fa-arrow-circle-left" style="font-size: 20px;"></i></span>
                    </a>
                    <a  href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span aria-hidden="true"><i class="fas fa-arrow-circle-right" style="font-size: 20px;"></i></span>
                    </a>
                </center>
            </div>
        </div>
    </div>
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <center><h5>Total Recovered</h5></center>
            <canvas id="myChart"></canvas>
            </div>
            <div class="carousel-item">
                <center><h5>Total Death</h5></center>
            <canvas id="getDeath"></canvas>
            </div>
            <div class="carousel-item">
                <center><h5>Total Confirmed</h5></center>
            <canvas id="getHealth"></canvas>
            </div>
        </div>
    </div>
  </div>
    <div class="col-3 mt-2">
      <div class="card mb-3 bg-info">
      <div class="border-bottom p-2">
          <center><h4>Total</h4></center>
          <center><?php echo number_format($temp_plus); ?>  Soul</center>
        </div>
        <div class="card-body">
            <div class="col-6 float-left border-right">
                <center><h5>Persentage Cure</h5></center>
                <center><?php echo substr($temp_Recovered,0,4); ?> %</center>
            </div>
            <div class="col-6 float-left">
                <center><h5>Persentage Deaths</h5></center>
                <center><?php echo substr($temp_Deaths,0,4); ?> %</center>
            </div>
        </div>
      </div>
      <div>
      <div class="card border border-dark">
        <div class="card-header">
          <center>Affected country</center> 
        </div>
        <div class="card-body">
          <blockquote class="blockquote mb-0">
             <center><i class="fas fa-globe"> Country : <?php echo $temp_arr; ?></i></center>
          </blockquote>
        </div>
      </div>
        <i>coronavirus is a type of virus. There are many types, and some cause disease. The newly identified coronavirus, SARS-CoV-2, has caused a worldwide respiratory disease pandemic, called COVID-19.</i><br><br>
        <center><a href="https://www.hopkinsmedicine.org/health/conditions-and-diseases/coronavirus" type="Submit" class="btn btn-outline-secondary">Read More</a></center>
      </div>
     </div>
  </div>
</div>


  
<script>
        var ctx = document.getElementById('myChart').getContext('2d')
        var cty = document.getElementById('getDeath').getContext('2d')
        var ctz = document.getElementById('getHealth').getContext('2d')

        var covid = $.ajax({
            url: "https://api.covid19api.com/summary",
            cache : false
        })
        
        .done(function (canvas) {
            
            
            function getContries(canvas) {
                var show_country=[];
                
                canvas.Countries.forEach(function(el) {
                    show_country.push(el.Country);
                })
                return show_country;
            }
            
            
            function getHealth(canvas) {
                var recovered=[];
                
                canvas.Countries.forEach(function(el) {
                    recovered.push(el.TotalConfirmed)
                })
                return recovered;
            }

            function getDeath(canvas) {
              var Death=[];

              canvas.Countries.forEach(function(el) {
                Death.push(el.TotalDeaths)
              })
              return Death;
            }    
            
            var colors = [];
            function getRandomColor(){
                var r = Math.floor(Math.random() * 225);
                var g = Math.floor(Math.random() * 225);
                var b = Math.floor(Math.random() * 225);
                return "rgb(" + r + "," + g + "," + b + ")";
            }
            for (var i in canvas.Countries){
                colors.push(getRandomColor());
            }
            
            var myChart = new Chart(ctx,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# of Votes',
                        data: getHealth(canvas),
                        backgroundColor:colors,
                        borderColor:colors,
                        borderWidth: 1,
                    }]
                },
                options: {
                  legend: {
                    display:false
                  }
                }
            })

            var getDeath = new Chart(cty,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# of Votes',
                        data: getDeath(canvas),
                        backgroundColor:colors,
                        borderColor:colors,
                        borderWidth: 1,
                    }]
                },
                options: {
                  legend: {
                    display:false
                  }
                }
            })

            var getHealth = new Chart(ctz,{
                type: 'bar',
                data: {
                    labels: getContries(canvas),
                    datasets : [{
                        label: '# of Votes',
                        data: getHealth(canvas),
                        backgroundColor:colors,
                        borderColor:colors,
                        borderWidth: 1,
                    }]
                },
                options: {
                  legend: {
                    display:false
                  }
                }
            })
            
        });
    </script>
   
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>